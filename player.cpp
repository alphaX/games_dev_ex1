/*
 * player.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */

#include "player.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

Player::Player(float speed, float angleSpeed){
	this->direction = STOP;
	this->turnDirection = NONE;
	this->speed = speed;
	this->angleSpeed = angleSpeed;
}

void Player::advance(Direction direction){
	this->direction = direction;
	this->step();
}

void Player::turn(TurnDirection turnDirection){
	this->turnDirection = turnDirection;
}

void Player::stop(){
	this->direction = STOP;
}

void Player::stopTurning(){
	this->turnDirection = NONE;
}

void Player::step(){
	switch(this->direction){
	case FORWARD:
		this->position += glm::rotate(glm::vec2(0.0,1.0), this->currentAngle)*this->speed;
		break;
	case BACKWARDS:
		this->position -= glm::rotate(glm::vec2(0.0,1.0), this->currentAngle)*this->speed;
		break;
	}
}

glm::vec2  Player::getPosition(){
	return this->position;
}

