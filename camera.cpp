/*
 * camera.cpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */

#include "camera.hpp";

Camera::Camera(Player * player){
	this->player = player;
}

glm::mat4 Camera::getViewMatrix(){
	glm::mat4 viewMatrix(1);
	viewMatrix = glm::translate(viewMatrix, -this->player->getPosition().x, -1.0f, -this->player->getPosition().y);

	return viewMatrix;
}



