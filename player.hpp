/*
 * player.hpp
 *
 *  Created on: Mar 9, 2013
 *      Author: alex
 */
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

enum Direction {
	STOP,
	FORWARD,
	BACKWARDS,
	LEFT,
	RIGHT
} ;

typedef enum{
	NONE,
	CLOCKWISE,
	ANTI_CLOCKWISE
} TurnDirection;

class Player{
	Direction direction;
	TurnDirection turnDirection;
	glm::vec2 position;
	float currentAngle;
	float speed;
	float angleSpeed;

public:
	Player(float speed, float angleSpeed);
	void advance(Direction direction);
	void turn(TurnDirection turnDirection);
	void stop();
	void stopTurning();
	void step();
	glm::vec2 getPosition();
};


#endif /* PLAYER_HPP_ */
