#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <GL/glew.h>
#include <GL/glfw.h>

// Cg headers
#include <Cg/cg.h>
#include <Cg/cgGL.h>

// GLM headers
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp> // for glm::value_ptr

#include "player.hpp"
#include "camera.hpp"

CGcontext		cgContext;
CGprogram		cgProgram;
CGprofile		cgVertexProfile;
CGparameter		shaderWVP;
CGerror			cgError;

glm::mat4 w = glm::mat4(1.0f);
glm::mat4 v = glm::mat4(1.0f);
glm::mat4 p = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.f);

glm::mat4 wvp = w*v*p;

typedef struct{
	GLuint verticesBuffer;
	GLuint indexesBuffer;
	int numberOfIndexes;
} MeshBuffer;


void initGL() {
	if( !glfwInit() ) {
		fprintf( stderr, "Failed to initialise GLFW\n" );
		exit(EXIT_FAILURE);
	}

	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT,GL_FALSE);

	// Open a window and create its OpenGL context
	if( !glfwOpenWindow( 1024, 768, 0,0,0,0, 32,0, GLFW_WINDOW ) ) {
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n" );
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	// Initialise GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		exit(EXIT_FAILURE);
	}


	glfwSetWindowTitle( "CGP Tutorial 01" );

	// Ensure we can capture the escape key being pressed below
	glfwEnable( GLFW_STICKY_KEYS );

	glClearColor(0.1f, 0.15f, 0.1f, 0.0f);
}

void initCg (const char * vertex_file_path) {

	if(!(cgContext = cgCreateContext())) {
		fprintf(stderr, "Failed to create Cg Context\n");
		exit(EXIT_FAILURE);
	}

	/* Output information about available profiles */
	printf("vertex profile:   %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_VERTEX)));
	printf("geometry profile: %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_GEOMETRY)));
	printf("fragment profile: %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_FRAGMENT)));
	printf("tessellation control profile: %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_TESSELLATION_CONTROL)));
	printf("tessellation evaluation profile: %s\n", cgGetProfileString(cgGLGetLatestProfile(CG_GL_TESSELLATION_EVALUATION)));


	if((cgVertexProfile = cgGLGetLatestProfile(CG_GL_VERTEX)) == CG_PROFILE_UNKNOWN) {
		fprintf(stderr, "Invalid profile type\n");
		exit(EXIT_FAILURE);
	}

	cgGLSetContextOptimalOptions(cgContext, cgVertexProfile);

	/* compile the Vertex Shader from file */
	if(!(cgProgram = cgCreateProgramFromFile(cgContext, CG_SOURCE, vertex_file_path, cgVertexProfile, "main", 0))) {
		fprintf(stderr, "%s\n", cgGetErrorString(cgGetError()));
		exit(EXIT_FAILURE);
	}

	cgGLLoadProgram(cgProgram);

	// Get handles to parameters in the shader so that we can access them from CPU-side code
	shaderWVP	= cgGetNamedParameter(cgProgram, "wvp");
}

void drawBufferedMesh(MeshBuffer mesh){
	glBindBuffer(GL_ARRAY_BUFFER, mesh.verticesBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.indexesBuffer);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(
			0,                  // attribute number; must match the layout in the shader
			3,                  // size
			GL_FLOAT,           // type
			GL_FALSE,           // normalised
			0,                  // stride
			(void*)0            // array buffer offset
	);

	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glDrawElements(GL_TRIANGLES, mesh.numberOfIndexes, GL_UNSIGNED_INT, (GLvoid*)0);
	glDisableVertexAttribArray(0);
}

MeshBuffer createMeshBuffer(GLfloat vertices[], GLuint indexes[], int numberOfvertices, int numberOfIndexes){
	GLuint vertexbuffer;
	glGenBuffers(1, &vertexbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * numberOfvertices * 3, vertices, GL_STATIC_DRAW);

	GLuint indexbuffer;
	glGenBuffers(1, &indexbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * numberOfIndexes, indexes, GL_STATIC_DRAW);

	MeshBuffer meshBuffer;
	meshBuffer.indexesBuffer = indexbuffer;
	meshBuffer.verticesBuffer = vertexbuffer;
	meshBuffer.numberOfIndexes = numberOfIndexes;

	return meshBuffer;
}

void cleanMeshBuffer(MeshBuffer meshBuffer){
	glDeleteBuffers(1, &(meshBuffer.verticesBuffer));
	glDeleteBuffers(1, &(meshBuffer.indexesBuffer));
}

MeshBuffer createPlaneMeshBuffer(int gridSizeX, int gridSizeZ, float gridDensityX, float gridDensityZ){
	// create vertices
	float numberOfCellsX = gridSizeX/gridDensityX;

	float numberOfCellsZ = gridSizeZ/gridDensityZ;
	int numberOfCells =  numberOfCellsX * numberOfCellsZ;
	int numberOfvertices = (numberOfCellsX + 1)  * (numberOfCellsZ + 1);

	GLfloat vertices [numberOfvertices * 3];

	int i = 0;
	for(float x = -gridSizeX/2.0; x<= gridSizeX/2.0; x+=gridDensityX){
		for(float z = -gridSizeZ/2.0; z <= gridSizeZ/2.0; z+=gridDensityZ){
			vertices[i] = x;
			vertices[i+1] = 0;
			vertices[i+2] = z;
			i += 3;
		}
	}

	// create indexes
	int numberOfIndexes = numberOfCells * 2 * 3;
	GLuint indexes[numberOfIndexes];
	int numberOfVerticesZ = numberOfCellsZ + 1;

	int j = 0;
	for(int iX = 0; iX < numberOfCellsX; iX++){
		for(int iZ = 0; iZ < numberOfCellsZ; iZ++){
			indexes[j] = iX + numberOfVerticesZ * iZ;
			indexes[j+1] = iX + numberOfVerticesZ * iZ + 1;
			indexes[j+2] = iX + numberOfVerticesZ * (iZ + 1);

			indexes[j+3] = iX + numberOfVerticesZ * iZ + 1;
			indexes[j+4] = iX + numberOfVerticesZ * (iZ + 1);
			indexes[j+5] = iX + numberOfVerticesZ * (iZ + 1) + 1;

			j+= 6;
		}
	}

	return createMeshBuffer(vertices, indexes, numberOfvertices, numberOfIndexes);
}

MeshBuffer createPyramidMeshBuffer(){
	GLfloat vertices[] = {
	 -1/2.0,  0.0f, -1/2.0,
	 1/2.0,  0.0f, -1/2.0,
	 -1/2.0,  0.0f, 1/2.0,
	 1/2.0,  0.0f, 1/2.0,
	 0.0f, 1.0f, 0.0f
	};

	GLuint indices[] = {
	   0,1,3,
	   0,2,3,
	   0,2,4,
	   0,1,4,
	   1,3,4,
	   2,3,4
	};

	return createMeshBuffer(vertices, indices, 5, 18);
}


int main( void ) {
    initGL();
	initCg("./vertex.cg");

	float tz = 0;
	float ty = 0;

	MeshBuffer planeBuffer = createPlaneMeshBuffer(30,30,0.2,0.2);
	MeshBuffer pyramidBuffer = createPyramidMeshBuffer();

	Player * player = new Player (0.1,0.1);
	Camera camera(player);

	do {
		int fps = 30;
		int interval = 1000000/fps;
		usleep(interval);

		if(glfwGetKey( GLFW_KEY_UP ) != GLFW_PRESS)
			player->advance(FORWARD);
		if(glfwGetKey( GLFW_KEY_DOWN ) != GLFW_PRESS)
			player->advance(BACKWARDS);

		v = camera.getViewMatrix();

		wvp = p*v*w;

		// Clear the screen
		glClear( GL_COLOR_BUFFER_BIT );

		// pass wvp matrix to shader

		cgGLSetMatrixParameterfc(shaderWVP, glm::value_ptr(wvp));

		cgGLEnableProfile(cgVertexProfile);

		/* Bind our Vertex Program to the Current State */
		cgGLBindProgram(cgProgram);

		drawBufferedMesh(planeBuffer);
		drawBufferedMesh(pyramidBuffer);

		glfwSwapBuffers();

	} while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS && glfwGetWindowParam( GLFW_OPENED ) );

	glfwTerminate();

	// Cleanup
	cleanMeshBuffer(planeBuffer);
	cleanMeshBuffer(pyramidBuffer);
	cgDestroyContext(cgContext);
	exit(EXIT_SUCCESS);
}

